package models

import anorm._
import anorm.SqlParser._
import play.api.db._
import play.api.Play.current


case class Owner(email: String, password: String)

object Owner {

	val simple = {
	  get[String]("email") ~ 
	  get[String]("password") map {
	    case email~password => Owner(email, password)
	  }
	}

	  /**
   * Authenticate a User.
   */
  def authenticate(email: String, password: String): Option[Owner] = {
    DB.withConnection { implicit connection =>
      SQL(
        """
         select * from owner where 
         email = {email} and password = {password}
        """
      ).on(
        'email -> email,
        'password -> password
      ).as(Owner.simple.singleOpt)
    }
  }

    def create(user : Owner) {
	  	DB.withConnection { implicit c =>
	    	SQL("insert into owner (email,password) values ({email},{password})").on(
	      		'email -> user.email,
	      		'password -> user.password
	    	).executeUpdate()
	  	}
	}

	def findByEmail(email: String): Option[Owner] = {
	    DB.withConnection { implicit connection =>
	      SQL("select * from owner where email = {email}").on(
	        'email -> email
	      ).as(Owner.simple.singleOpt)
	    }
  	}


}