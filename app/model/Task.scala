package models

import java.util.{Date}
import anorm._
import anorm.SqlParser._
import play.api.db._
import play.api.Play.current


case class Task(id: Pk[Long] = NotAssigned, label: String, discontinued: Option[Date], user_email: String)

object Task {
	var order=0

	def all(): List[Task] = DB.withConnection { implicit c =>
	  SQL("select * from task").as(task *)
	}

	def change(){
		order=1
	}

	def orderByDiscontinued(): List[Task] = DB.withConnection { implicit c =>
	  SQL("select * from task order by discontinued").as(task *)
	}

	def create(task : Task) {
	  DB.withConnection { implicit c =>
	    SQL("insert into task (label,discontinued,user_email) values ({label},{discontinued},{user_email})").on(
	      'label -> task.label,
	      'discontinued -> task.discontinued,
	      'user_email -> task.user_email
	    ).executeUpdate()
	  }
	}

	def delete(id: Long) = {
	  DB.withConnection { implicit c =>
	    SQL("delete from task where id = {id}").on(
	      'id -> id
	    ).executeUpdate()
	  }
	}

	def findById(id: Long): Option[Task] = {
	    DB.withConnection { implicit connection =>
	      SQL("select * from task where id = {id}").on('id -> id).as(Task.task.singleOpt)
	    }
  	}

  	def update(id: Long, task: Task) = {
	    DB.withConnection { implicit connection =>
	      SQL(
	        """
	          update task
	          set label = {label}, discontinued = {discontinued}
	          where id = {id}
	        """
	      ).on(
	        'id -> id,
	        'label -> task.label,
	        'discontinued -> task.discontinued
	        //'user_email -> task.user_email
	      ).executeUpdate()
	    }
	}

	def findInvolving(user: String): List[Task] = {
		if(order==0){
			order=1
			DB.withConnection { implicit connection =>
		      SQL(
		        """
		          select * from task  
		          where user_email = {email} order by discontinued
		        """
		      ).on(
		        'email -> user
		      ).as(Task.task *)
		    }

		}
		else
		{
			order=0
			DB.withConnection { implicit connection =>
			      SQL(
			        """
			          select * from task  
			          where user_email = {email} order by id
			        """
			      ).on(
			        'email -> user
			      ).as(Task.task *)
			    }
					}
			    
			  }

    def isOwner(task: Long, user: String): Boolean = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          select count(task.id) = 1 from task 
          where user_email = {email} and id = {task}
        """
      ).on(
        'task -> task,
        'email -> user
      ).as(scalar[Boolean].single)
    }
  }

	val task = {
	  get[Pk[Long]]("id") ~ 
	  get[String]("label") ~
	  get[Option[Date]]("discontinued") ~
	  get[String]("user_email") map {
	    case id~label~discontinued~user_email => Task(id, label, discontinued, user_email)
	  }
	}

}