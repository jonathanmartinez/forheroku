package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import anorm._
import views._
import models._

object Application extends Controller {


	def index = IsAuthenticated { username => _ =>
	    Owner.findByEmail(username).map { user =>
	      Ok(
	      	html.index(
	        	Task.findInvolving(username),
	        	username 
	        )
	      )
	    }.getOrElse(Forbidden)
	}

	/*def index = Action {
  		Redirect(routes.Application.tasks(order))
  	}
  	
  	var order = 0

  	def tasks (orderBy: Int=order) = Action {
  		order=orderBy
  		if(order==0){
  			Ok(views.html.index(Task.all()))	
  		} 
  		else{
  			Ok(views.html.index(Task.orderByDiscontinued()))
  		}
  	}*/
  	
  	def newTask(user:String) = Action {
  		Ok(views.html.newTask(taskForm,user))
  	}

  	def editTask(id: Long, user:String) =  Action {
	    Task.findById(id).map { task =>
	      Ok(html.tasks(id, taskForm.fill(task),user))
	    }.getOrElse(NotFound)
	  }

  	/*def updateTask(id: Long, user: String) = Action { implicit request =>
	    taskForm.bindFromRequest.fold(
	      formWithErrors => BadRequest(views.html.tasks(id,formWithErrors,user)),
	      task => {
	        Task.update(id, task)
	        //Redirect(routes.Application.tasks(order))
	        Redirect(routes.Application.index)
	      }
	    )
	}*/

	def updateTask(id: Long) =  IsOwnerOf(id) { user => implicit request =>
   taskForm.bindFromRequest.fold (
      formWithErrors => BadRequest(views.html.tasks(id, formWithErrors,user)),
      task => {
         Task.update(id, task)
         Redirect(routes.Application.index)
      }
   )
}


	def createTask = IsAuthenticated { username => implicit request =>

	      taskForm.bindFromRequest.fold(
	    	errors => BadRequest(views.html.newTask(taskForm,username)),//preguntar lo de quitar errors de los parametros
	    	task => {
	      		Task.create(task)
	      		//Redirect(routes.Application.tasks(order))
	      		Redirect(routes.Application.index)
	    	}
  		)

	}
  	/*def createTask(user: String) = Action { implicit request =>
  		taskForm.bindFromRequest.fold(
	    	errors => BadRequest(views.html.newTask(taskForm,user)),//preguntar lo de quitar errors de los parametros
	    	task => {
	      		Task.create(task)
	      		//Redirect(routes.Application.tasks(order))
	      		Redirect(routes.Application.index)
	    	}
  		)
	}*/
  
  	def deleteTask(id: Long) = Action {
	  	Task.delete(id)
	  	//Redirect(routes.Application.tasks(order))
	  	Redirect(routes.Application.index)
	}

	def login = Action { implicit request =>
    	Ok(views.html.login(loginForm))
  	}

  	def signin = Action { implicit request =>
    	Ok(views.html.signin(signInForm))
  	}

  	def createUser = Action { implicit request =>
  		signInForm.bindFromRequest.fold(
	    	errors => BadRequest(views.html.signin(signInForm)),//preguntar lo de quitar errors de los parametros
	    	user => {
	      		Owner.create(user)
	      		Owner.authenticate(user.email,user.password)
	      		//Redirect(routes.Application.tasks(order)).withSession("email" -> user.email)
	      		Redirect(routes.Application.index).withSession("email" -> user.email)
	    	}
  		)
	}

	def authenticate = Action { implicit request =>
	    loginForm.bindFromRequest.fold(
	      formWithErrors => BadRequest(html.login(formWithErrors)),
	      user => Redirect(routes.Application.index).withSession("email" -> user._1)//Redirect(routes.Application.tasks(order)).withSession("email" -> user._1)
	    )
	}

	def logout = Action {
	    Redirect(routes.Application.login).withNewSession.flashing(
	      "success" -> "You've been logged out"
	    )
	}

	val taskForm = Form(
		mapping(
			"id" -> ignored(NotAssigned:Pk[Long]),
			"label" -> nonEmptyText,
			"discontinued" -> optional(date("yyyy-MM-dd")),
			"user_email" -> text
		)(Task.apply)(Task.unapply)
    
  	)

  	val signInForm = Form(
		mapping(
			"email" -> nonEmptyText,
			"password" -> nonEmptyText
		)(Owner.apply)(Owner.unapply)
  	)

	val loginForm = Form(
	    tuple(
	      "email" -> text,
	      "password" -> text
	    ) verifying ("Invalid email or password", result => result match {
	      case (email, password) => Owner.authenticate(email, password).isDefined
	    })
  	)	 

  	/**
* Retrieve the connected user email.
*/
private def username(request: RequestHeader) = request.session.get("email")

/**
* Redirect to login if the user in not authorized.
*/
private def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Application.login)


  def IsAuthenticated(f: => String => Request[AnyContent] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
    Action(request => f(user)(request))
  }

/** 
* Check authenticated user
*/
def GetLoggedUser (f: => String => Request[AnyContent] => Result) = 
   Security.Authenticated(username, onUnauthorized) { 
      user => Action(request => f(user)(request))
}

/*
* Check if the connected user is a owner of the task
*/

def IsOwnerOf(task: Long)(f: => String => Request[AnyContent] => Result) = 
   GetLoggedUser { user => request =>
      if(Task.isOwner(task, user)) {
         f(user)(request)
      } else {
         Results.Forbidden
      }
   }
	
}