#Información

* Alumno: Jonathan Martínez López
* DNI: 74365136H
* Heroku: `Error al desplegar`

#Índice
1. Introducción. Nuevas funcionalidades.
1. Paso 1. Nueva evolución de la base de datos.
1. Paso 2. Nuevo modelo User.
1. Paso 3. Log in y cookie de sesión.
1. Paso 4. Log out.
1. Paso 5. Registro de nuevos usuarios.
1. Paso 6. Restringir el acceso a otros usuario.
1. Aspectos técnicos.


#Introducción. Nuevas funcionalidades

En esta tercera práctica vamos a dotar a la aplicación de nuevas funcionalidades con el objetivo de ir escribiendo código en Scala y Play Framework por nosotros mismos e ir subiendo las diferentes versiones a Bitbucket y Heroku a medida que vamos avanzando. Las nuevas funcionalidades que incluiremos a la aplicación son las siguientes:

* Registro de usuarios y autenticación
* Acceso restringido a las tareas, cada usuario ve únicamente las suyas
* Sólo podrá crear, editar o eliminar tareas suyas.

Veamos como hemos ido añadiendo estas funcionalidades mediante Scala y Play Framework.

#Paso 1. Nueva evolución de la base de datos.

Para poder implementar las nuevas funcionalidades debemos hacer modificaciones en los modelos de la aplicación, por lo que vamos a comenzar por añadir una nueva tabla `user` (User me parecía más descriptivo que Owner) en la BD y una nueva columnda en la tabla `task` que haga referencia (Clave ajena) al usuario propietario de esa tarea.

Para ello creamos un nuevo archivo llamado 2.sql en:

	conf/evolutions/default/2.sql

que contenga las nuevas sentencias sql:

	# Users schema
 
	# --- !Ups

	CREATE TABLE user (
	    email varchar(255) NOT NULL,
	    password varchar(255) NOT NULL,
	    PRIMARY KEY (email)
	);

	ALTER TABLE task ADD user_email varchar(255);
	INSERT INTO user(email,password) VALUES('admin','admin');
	ALTER TABLE task ADD FOREIGN KEY (user_email) REFERENCES user(email);

	# --- !Downs
	 
	DROP TABLE user;
	ALTER TABLE user DROP email;

Como vemos, hemos añadido también un usuario de pruebas `admin` para ir probando que el `Log in` funciona correctamente antes de hacer el `Sign in`. Eso es todo hasta este punto, veamos ahora lo siguiente.

#Paso 2. Nuevo modelo User.

Ahora creamos el nuevo modelo `User` que representará a los usuarios de la aplicación. Este modelo tendrá dos campos: `email` que sera el identificador único y `password` que será la contraseña para acceder al sistema.

case class User(email: String, password: String)

	object User {

		val simple = {
		  get[String]("email") ~ 
		  get[String]("password") map {
		    case email~password => User(email, password)
		  }
		}
	}


#Paso 3. Login y cookie de sesión.

Lo primere que debemos hacer para imnplementar el sistema de login es crear una vista con un formulario que nos pida el email y contraseña del usuario, para comprobarlo contra la base de datos y almacenar en una cookie la sesión del usuario.

Comenzamos por crear la vista con el formulario `login.scala.html`:

	@(loginForm: Form[(String,String)])(implicit flash: Flash)

	@import helper._

	@main("Todo list") {
		<div class="row-fluid">
			<div class="span12 pagination-centered well">
				<h2>Log in</h2>
				@form(routes.Application.authenticate) {
					@inputText(loginForm("email"),'_email -> "Email")
					@inputText(loginForm("password"),'_password -> "Password") 
					<button class="btn btn-success" type="submit" value="Create"><i class="icon-ok icon-white"></i> Log in</button> or
					<a href="@routes.Application.signin" class="btn"><i class="icon-user"></i> Sign in</a
				}
			</div>
		</div>	
		
	}

Creamos pues el controlador que responderá a la acción de pulsar el botón `Log in` y guardará la cookie de sesión:

	def authenticate = Action { implicit request =>
	    loginForm.bindFromRequest.fold(
	      formWithErrors => BadRequest(html.login(formWithErrors)),
	      user => Redirect(routes.Application.index).withSession("email" -> user._1)//Redirect(routes.Application.tasks(order)).withSession("email" -> user._1)
	    )
	}

y modificamos el index para que sólo muestre las tareas asociadas a el usuario en sesión:

	def index = IsAuthenticated { username => _ =>
	    User.findByEmail(username).map { user =>
	      Ok(
	      	html.index(
	        	Task.findInvolving(username),
	        	username 
	        )
	      )
	    }.getOrElse(Forbidden)
	}

Como podemos ver hemos creado un método `findInvolving(username)` que nos devuelve únicamente las tareas asociadas al usuario en sesión:

	def findInvolving(user: String): List[Task] = {
	    DB.withConnection { implicit connection =>
	      SQL(
	        """
	          select * from task  
	          where user_email = {email}
	        """
	      ).on(
	        'email -> user
	      ).as(Task.task *)
	    }
	}

#Paso 4. Log out

Una vez hecho el Log in necesitamos hacer el proceso inverso (Log out) para que los usuarios puedan salir de su sesión y entrar con otro usuario. Para ello en la vista de tareas de cada usuario añadimos el siguiente enlace:

 	<a href="@routes.Application.logout()">Logout</a>

que llamará a la siguiente Action, la cual te redirecciona hacia el login y borra el valor de la cookie de sesión(el email del usuario):

	def logout = Action {
	    Redirect(routes.Application.login).withNewSession.flashing(
	      "success" -> "You've been logged out"
	    )
	}

#Paso 5. Registro de nuevos usuarios.

Llegado a este punto notamos la necesidad de que la aplicación nos permita crear nuecos usuarios, ya que hasta ahora hemos estado utilizando el usuario de pruebas `admin` que hemos insertado directamente en la base de datos. 

Comencemos creando una vista de alta de usuario `signin` que contiene un formulario con el email y password del nuevo usuario:

	@(loginForm: Form[User])

	@import helper._

	@main("Todo list") {
		<div class="row-fluid">
			<div class="span12 pagination-centered well">
				<h2>Sign in</h2>
				@form(routes.Application.createUser) {
					@inputText(loginForm("email"),'_email -> "Email")
					@inputText(loginForm("password"),'_password -> "Password") 
					<button class="btn btn-success" type="submit" value="Create"><i class="icon-ok icon-white"></i> Sign in</button> or
					<a href="@routes.Application.login" class="btn">Cancel</a>
				}
			</div>
		</div>	
		
	}

Dicho formulario lanza la `Action` `createUser` que se encarga de crear un nuevo usuario a través del método `create` del modelo `User` y de autentificar el usuario creado al mismo tiempo:

  	def createUser = Action { implicit request =>
  		signInForm.bindFromRequest.fold(
	    	errors => BadRequest(views.html.signin(signInForm)),//preguntar lo de quitar errors de los parametros
	    	user => {
	      		User.create(user)
	      		User.authenticate(user.email,user.password)
	      		//Redirect(routes.Application.tasks(order)).withSession("email" -> user.email)
	      		Redirect(routes.Application.index).withSession("email" -> user.email)
	    	}
  		)
	}

    def create(user : User) {
	  	DB.withConnection { implicit c =>
	    	SQL("insert into user (email,password) values ({email},{password})").on(
	      		'email -> user.email,
	      		'password -> user.password
	    	).executeUpdate()
	  	}
	}



#Paso 6. Restringir el acceso a otros usuario.

Finalmente, vamos a controlar que un usuario no pueda acceder directamente a una ruta que represente las tareas de otro usuario para editarlas, crearlas o borrarlas. Para ello debemos apoyarnos en los métodos:

	 	/**
	* Retrieve the connected user email.
	*/
	private def username(request: RequestHeader) = request.session.get("email")

	/**
	* Redirect to login if the user in not authorized.
	*/
	private def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Application.login)


	  def IsAuthenticated(f: => String => Request[AnyContent] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
	    Action(request => f(user)(request))
	  }

	/** 
	* Check authenticated user
	*/
	def GetLoggedUser (f: => String => Request[AnyContent] => Result) = 
	   Security.Authenticated(username, onUnauthorized) { 
	      user => Action(request => f(user)(request))
	}

	/*
	* Check if the connected user is a owner of the task
	*/

	def IsOwnerOf(task: Long)(f: => String => Request[AnyContent] => Result) = 
	   GetLoggedUser { user => request =>
	      if(Task.isOwner(task, user)) {
	         f(user)(request)
	      } else {
	         Results.Forbidden
	      }
	   }
		
	}

Dichos métodos comprueban que el usuario logueado es el propietario de la tarea que se está intentado modificar o eliminar. Veamos un ejemplo de como quedaría el modificar una tarea con acceso restringido al usuario logueado:

	def updateTask(id: Long) =  IsOwnerOf(id) { user => implicit request =>
	   taskForm.bindFromRequest.fold (
	      formWithErrors => BadRequest(views.html.tasks(id, formWithErrors,user)),
	      task => {
	         Task.update(id, task)
	         Redirect(routes.Application.index)
	      }
	   )
	}

Eso es todo acerca de las nuevas funcionalidades de la aplicación, veamos ahora una explicación de los aspectos tecnicos introducidos.


#Aspectos técnicos.

Una de las novedades que hemos utilizado en esta práctica es el uso de cookies para mantener la sesión del usuario. Para ello, cuando realizamos una petición `HTTP` de tipo `POST` desde el formulario de login, se llama a la Action `authenticate` que comprueba contra la base de datos que el usuario y contraseña son correctos y finalmente almacena el valor del email que llega en el formulario(primer campo, user._1) en una cookie con: withSession("email" -> user._1).

A partir de aquí la aplicación hace comparaciones con el valor de la cookie para restringir el acceso a las secciones de creación, modificación y borrado de tareas, así como al listado de tareas para mostrarle a cada usuario únicamente las suyas.

Otro aspecto técnico utilizado en la aplicación es la combinación de:

* Parámetros by-name en Scala
* Tipo A => B => C

Los parámetros by-name de Scala son expresiones que devuelven un tipo de dato y que no son evaluadas al ser llamadas, sino en su cuerpo.

Por otro lado, el tipo A => B => C representa una función que recibe un dato del tipo A y devuelve una función que a su vez recibe un dato del tipo B y devuelve un dato del tipo C.

Este concepto ha sido utilizado, por ejemplo, en la expresión `isOwnerOf`:

	def IsOwnerOf(task: Long)(f: => String => Request[AnyContent] => Result) = 
	   GetLoggedUser { user => request =>
	      if(Task.isOwner(task, user)) {
	         f(user)(request)
	      } else {
	         Results.Forbidden
	      }
	   }
		
	}

Dicho método comprueba si el usuario que hay en sesión es el propietario de la tarea que se le pasa por parámetro y para ello hace uso de los conceptos comentados anteriormente. El tipo A => B => C recibe inicialmente un string con el usuario que debe comprobar si es el propietario de la tarea.



