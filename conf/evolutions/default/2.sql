# Users schema
 
# --- !Ups

CREATE TABLE owner (
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    PRIMARY KEY (email)
);

ALTER TABLE task ADD user_email varchar(255);
INSERT INTO owner(email,password) VALUES('admin','admin');
ALTER TABLE task ADD FOREIGN KEY (user_email) REFERENCES owner(email);

# --- !Downs
 
DROP TABLE owner;
ALTER TABLE task DROP email;